'use strict'

// global variables
let currentProduct = document.getElementsByClassName('gallery-view')[0];
let currentProductModel = document.getElementsByClassName('model')[0].getElementsByTagName('span')[0];
let productLinks = document.getElementsByTagName('a');

// functions
function showProduct(event) {
  
  event.preventDefault();
  
  for (let item of productLinks) {
    item.classList.remove('gallery-current');
  }
  
  event.currentTarget.classList.add('gallery-current');
  currentProduct.src = event.currentTarget.href;
  currentProductModel.innerText = event.currentTarget.getElementsByTagName('img')[0].title;
}

// events
Array.from(productLinks).forEach(link => {
  link.addEventListener('click', showProduct);
})