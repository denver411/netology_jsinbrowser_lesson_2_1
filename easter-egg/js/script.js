'use strict'

// global variables
let menu = document.getElementsByTagName('nav')[0];
let secretBlock = document.getElementsByClassName('secret')[0];
const secretWord = ['KeyY', 'KeyT', 'KeyN', 'KeyJ', 'KeyK', 'KeyJ', 'KeyU', 'KeyB', 'KeyZ'];
const userWord = [];

// functions
function openPanel(event) {

  if (event.ctrlKey && event.altKey && event.code === 'KeyT') {
    menu.classList.toggle('visible');
  }

}

function secretCode(event) {

  if (event.code === secretWord[userWord.length]) {
    userWord.push(event.code);
  } else {
    userWord.length = 0;
  }

  if (userWord.length === secretWord.length) {
    secretBlock.classList.add('visible');
  }

}

// events
document.addEventListener('keydown', openPanel);
document.addEventListener('keydown', secretCode);