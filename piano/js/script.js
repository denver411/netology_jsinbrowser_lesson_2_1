'use strict'
// global variables
const buttons = document.getElementsByTagName('li');

// functions
function buttonPlay(event) {
  let register = 'middle';
  let soundSet = document.getElementsByClassName('set')[0];
  const sounds = {
    0: 'first',
    1: 'second',
    2: 'third',
    3: 'fourth',
    4: 'fifth'
  }
  let currentSound = sounds[Array.from(buttons).indexOf(event.currentTarget)];
  let currentButton = event.currentTarget.getElementsByTagName('audio')[0];

  if (soundSet.classList.contains('middle')) {
    soundSet.classList.remove('middle');
  };
  if (soundSet.classList.contains('lower')) {
    soundSet.classList.remove('lower');
  };
  if (soundSet.classList.contains('higher')) {
    soundSet.classList.remove('higher');
  };
  if (event.shiftKey) {
    register = 'lower';
    soundSet.classList.add('lower');
  } else if (event.altKey) {
    register = 'higher';
    soundSet.classList.add('higher');
  } else {
    soundSet.classList.add('middle');
  }

  currentButton.src = `sounds/${register}/${currentSound}.mp3`;
  currentButton.pause();
  currentButton.currentTime = 0;
  currentButton.play();

}

// events
Array.from(buttons).forEach(button => {
  button.addEventListener('click', buttonPlay);
})